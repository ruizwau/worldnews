import React from "react";
import News from "./News";
import SearchSideBar from './SearchSideBar';
import { GlobalContext } from "../NewsContext/GlobalContext";
import axios from "axios";
import { useContext, useEffect,useState } from "react";


function Search() {
  const [url, seturl] = useState(`https://localhost:44379/api/news/search?keywords=peso`);
const {setData} = useContext(GlobalContext);

const HandleResponse = (p) => {
  var h = JSON.parse(p.headers['x-pagination']);
  setData(p.data)
};

useEffect(() => {
  axios
    .get(url)
    .then((response) => HandleResponse(response))
    .catch((error) => console.log(error));
}, [url]);

  return (
      <div className="news-body ">
        <SearchSideBar set={seturl}></SearchSideBar>
        <div className="newscontainer" >
          <News/>
        </div>
      </div>
  );
}

export default Search;