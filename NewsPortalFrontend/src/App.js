import React from "react";
import './App.css';
import Navbar from './components/Navbar';
import Search from './components/Search';
import Header from './components/AppHeader';
import TopN from './components/TopNews';
import { GlobalContextProvider } from "./NewsContext/GlobalContext";
import {
	BrowserRouter,
	Routes,
	Route,
} from "react-router-dom";
function App() {

	return (

			<GlobalContextProvider>
				<div className="App">
					<Header />
					<Navbar />
					<BrowserRouter>
						<Routes>
							<Route path='/' element={<TopN />}/>
							<Route path='/TopN' element={<TopN />}/>
							<Route path='/Search' element={<Search/>} />
						</Routes>
					</BrowserRouter>
				</div>
			</GlobalContextProvider>

	);
}

export default App;
