export function DinamicStyleTop(country,category){
    const list = document.querySelectorAll('.country')
    const listc = document.querySelectorAll('.category')
    list.forEach((item) => item.classList.add('w3-light-grey'))
    list.forEach((item) => item.classList.remove('w3-indigo'))
    listc.forEach((item) => item.classList.add('w3-light-grey'))
    listc.forEach((item) => item.classList.remove('w3-indigo'))
    const init = document.querySelector('#'+country);
    const cat = document.querySelector('#'+category);
    init.classList.add('w3-indigo')
    init.classList.remove('w3-light-grey')
    cat.classList.add('w3-indigo')
    cat.classList.remove('w3-light-grey')
}

export function DinamicStyleSearch(lang){
    const list = document.querySelectorAll('.country')
    list.forEach((item) => item.classList.add('w3-light-grey'))
    list.forEach((item) => item.classList.remove('w3-indigo'))
    const init = document.querySelector('#'+lang);
    init.classList.add('w3-indigo')
    init.classList.remove('w3-light-grey')
}