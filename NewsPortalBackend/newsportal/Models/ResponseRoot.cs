﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newsportal.Models
{
    public class Response
    {

        public string status { get; set; }
        public int totalResults { get; set; }
        public List<Article> articles { get; set; }

    }
}
