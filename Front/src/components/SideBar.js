import React, { useState } from "react";
import { GlobalContext } from "../NewsContext/GlobalContext";
import { useContext, useEffect } from "react";
import {DinamicStyleTop} from "../utils.js"


function SideBar(props) {

  const {setcountry,setcategory,country,category} = useContext(GlobalContext);
  const countryarray = [
    'ae','ar','at','au','be','bg','br','ca','ch','cn','co','cu','cz','de','eg','fr','gb','gr','hk','hu','id','ie','il','in','it','jp','kr','lt','lv','ma','mx','my','ng','nl','no','nz','ph','pl','pt','ro','rs','ru','sa','se','sg','si','sk','th','tr','tw','ua','us','ve','za'
  ];
  const categoryarray=['business','entertainment','generalhealth','science','sports','technology'];
	useEffect(()=>{
        DinamicStyleTop(country,category);
        props.set(`https://localhost:44379/api/news/top-headlines?country=${country}&category=${category}`);
	},[country,category])



  return (
    
    <div className="left-container">
    <div className="card w3-white w3-card-4 w3-margin-top">
        <header className="w3-container w3-padding w3-indigo">
            <h5>COUNTRIES</h5>
        </header>
        <div className="categories-tags w3-container">
            <p key={"btncount"}>
                {countryarray.map(c =><button key={c} id={c} className={ "w3-btn w3-light-grey w3-tag w3-margin-right w3-margin-bottom w3-border-0 country"} onClick={()=>setcountry(c)}>{c}</button>)}
            </p>
        </div>
    </div>

    <div className="card w3-white w3-card-4 w3-margin-top">
        <header className="w3-container w3-padding w3-indigo">
            <h5>CATEGORIES</h5>
        </header>
        <div className="categories-tags w3-container">
            <p>
                {categoryarray.map(c => <button key={c} id={c} className="w3-btn w3-tag w3-light-grey w3-margin-right w3-margin-bottom w3-border-0 category" onClick={()=>setcategory(c)}>{c}</button>)}
            </p>
        </div>
    </div>
    <div className="w3-white w3-margin-top">
        <div className="w3-container w3-padding w3-indigo">
            <h5>FOLLOW ME</h5>
        </div>
        <div className="w3-container w3-xlarge w3-padding">
            <i className="fa fa-facebook-official w3-hover-opacity" />
            <i className="fa fa-instagram w3-hover-opacity" />
            <i className="fa fa-snapchat w3-hover-opacity" />
            <i className="fa fa-pinterest-p w3-hover-opacity" />
            <i className="fa fa-twitter w3-hover-opacity" />
            <i className="fa fa-linkedin  w3-hover-opacity" />
        </div>
    </div>
    <div className="w3-white w3-margin-top">
        <div className="w3-container w3-padding w3-indigo">
            <h5>SUBSCRIBE</h5>
        </div>
        <div className="w3-container w3-white">
            <p>
                Enter your e-mail below and get notified on
                the latest news.
            </p>
            <p>
                <input
                    className="w3-input w3-border"
                    type="text"
                    placeholder="Enter e-mail"
                    style={{ width: 100 + '%' }}
                />
            </p>
            <p>
                <button
                    type="button"
                    className="w3-button w3-block w3-red"
                >
                    Subscribe
                </button>
            </p>
        </div>
    </div>
</div>
  );


}


export default SideBar;