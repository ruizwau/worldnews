import React, { useState } from "react";
import { useContext, useEffect } from "react";
import DatePicker from './DatePicker';
import moment from 'moment';
import { Formik, useFormik } from 'formik';
import { GlobalContext } from "../NewsContext/GlobalContext";
import {DinamicStyleSearch} from "../utils.js"
function SearchSideBar(props) {

    const leng = [
        'ar', 'de', 'en', 'es', 'fr', 'he', 'it', 'nl', 'no', 'pt', 'ru', 'sv', 'ud', 'zh'
    ];
    const { SeacrhTerm,
            language,
            dateFrom,
            dateTo,
            setdateFrom, 
            setdateTo, 
            setSeacrhTerm, 
            setlanguage} = useContext(GlobalContext);

    const[lang,setlang]=useState("es");
    useEffect(()=>{
        DinamicStyleSearch(lang);

    },[lang])           
    useEffect(()=>{
        props.set(`https://localhost:44379/api/news/search?keywords=${SeacrhTerm}&dateFrom=${dateFrom}&dateTo=${dateTo}&language=${language}`);
	},[SeacrhTerm,language,dateFrom,dateTo])



    const formik = useFormik({
        initialValues: {
            startDate: new Date(),
            endDate: new Date(),
            Term: ""
        }
    });

    return (

        <div className="left-container">

            <div className="w3-white w3-margin-top">
                <div className="w3-container w3-padding w3-indigo">
                    <h5>SEARCH</h5>
                </div>
                <div className="w3-container w3-padding">
                    <Formik
                        initialValues={formik.initialValues}
                        onSubmit={(values, actions) => {
                            setSeacrhTerm(values.Term);
                            setdateFrom(moment(values.startDate).format("YYYY-MM-DD"));
                            setdateTo(moment(values.endDate).format("YYYY-MM-DD"));
                            setlanguage(lang);
                        }}
                    >
                        {props => (
                            <form onSubmit={props.handleSubmit}>



                                <div className="w3-container w3-padding w3-margin-top">
                                    <label className="w3-tag w3-indigo  w3-margin">
                                        Search Term
                                    </label>
                                    <input id='Term'
                                        type='text'
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        value={props.values.name}
                                        name='Term'
                                        placeholder='ingresar término'
                                        className="w3-input w3-border-indigo w3-border-bottom" />
                                </div>

                                <div className="w3-container w3-padding w3-margin-top">
                                    <label className="w3-tag w3-indigo  w3-margin">
                                        Date From
                                    </label>
                                    <DatePicker
                                        value={props.values.dateFrom}
                                        name='startDate'
                                        dateFormat="yyyy-MM-dd" 
                                    />
                                </div>



                                <div className="w3-container w3-padding w3-margin-top">
                                    <label className="w3-tag w3-indigo  w3-margin">
                                        Date To
                                    </label>
                                    <DatePicker
                                        value={props.values.dateTo}
                                        name='endDate'
                                        dateFormat="yyyy-MM-dd" 
                                    />
                                </div>

                                {props.errors.name && (
                                    <div id='feedback'>{props.errors.name}</div>
                                )}

                                <div className="categories-tags w3-container w3-margin-top w3-margin-bottom">
                                    <label className="w3-tag w3-indigo  w3-margin">
                                        Preferred language
                                    </label>
                                    <p key={"btncount"}>
                                        {leng.map(c => <div
                                            type='button'
                                            id={c}
                                            key={c} className={"w3-button w3-btn w3-light-grey w3-tag w3-margin-right w3-margin-bottom w3-border-0 country" } onClick={(e) => setlang(c)} >{c}</div>)}
                                    </p>
                                </div>
                                <button type="submit" className="w3-btn w3-block w3-red">
                                    Search
                                </button>
                            </form>
                        )}
                    </Formik>
                </div>
            </div>

            <div className="w3-white w3-margin-top">
                <div className="w3-container w3-padding w3-indigo">
                    <h5>SUBSCRIBE</h5>
                </div>
                <div className="w3-container w3-white">
                    <p>
                        Enter your e-mail below and get notified on
                        the latest news.
                    </p>
                    <p>
                        <input
                            className="w3-input w3-border"
                            type="text"
                            placeholder="Enter e-mail"
                            style={{ width: 100 + '%' }}
                        />
                    </p>
                    <p>
                        <div
                            type="button"
                            className="w3-button w3-block w3-red"
                        >
                            Subscribe
                        </div>
                    </p>
                </div>
            </div>
        </div>
    );


}


export default SearchSideBar;