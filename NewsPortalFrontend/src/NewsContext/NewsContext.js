import React, { createContext, useEffect, useState } from "react";
import axios from "axios";
import moment from 'moment';

export const NewsContext = createContext();

export const NewsContextProvider = (props) => {

 
  const [data, setData] = useState();
  const [page, setpage] = useState(1);
  const [pagesize, setpagesize] = useState(10);
  const [totalpages, settotalpages] = useState(0);
  const [hasnext, sethasnext] = useState(false);
  const [hasprev, sethasprev] = useState(false);
  const [total, settotal] = useState(0);
  const[category, setcategory] = useState('business');
	const[country, setcountry] = useState('ar');
  const[SeacrhTerm, setSeacrhTerm] = useState("peso");
  const[language,setlanguage]=useState("es");
  const[dateFrom, setdateFrom] = useState(moment(new Date()).format("YYYY-MM-DD"));
  const[dateTo, setdateTo] = useState(moment(new Date()).format("YYYY-MM-DD"));
  
  const HandleResponse=(p)=>{
      var h=JSON.parse(p.headers['x-pagination']);
      setData(p.data)
      setpage(h.page);
      setpagesize(h.pagesize);
      settotalpages(h.totalPages);
      sethasnext(h.HasNext);
      sethasprev(h.HasPrevious);
      settotal(h.totalResults);
  };

  var url="";
  if(props.Route==="Search"){

     url=`https://localhost:44379/api/news/search?keywords=${SeacrhTerm}&dateFrom=${dateFrom}&dateTo=${dateTo}&language=${language}`;
  }
  else{
    url=`https://localhost:44379/api/news/top-headlines?country=${country}&category=${category}`;
  }
  useEffect(() => {
   
    console.log(data);
    console.log(page);
    console.log(pagesize);
    console.log(totalpages);
    console.log(total);
    console.log(hasnext);
    console.log(hasprev);
    axios
      .get(url)
      .then((response) => HandleResponse(response))
      .catch((error) => console.log(error));
  }, [country,category,SeacrhTerm,dateFrom,dateTo,language]);


  return (
    <NewsContext.Provider value={{ data,setcountry,setcategory,country,category,setdateFrom,setdateTo,setSeacrhTerm,setlanguage}}>
      {props.children}
    </NewsContext.Provider>
  );
};
