import React, { useContext, useEffect } from "react";
import NewsArticle from "./NewsArticle";
import { GlobalContext } from "../NewsContext/GlobalContext";
import axios from "axios";
function News(props) {

  const {data}=React.useContext(GlobalContext);

  return (
    <div>
      <div className="all__news w3-margin">
        {data
          ? data.articles.map((news) => (
            <NewsArticle data={news} key={news.url} />
          ))
          : <></>}
      </div>
    </div>
  );
}

export default News;
