using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Reflection;
using System.IO;
using AutoMapper;

namespace newsportal
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddHttpClient("top-headlines", c =>
            {
                c.BaseAddress = new Uri("https://newsapi.org/v2/top-headlines");
                c.DefaultRequestHeaders.Add("Accept", "*/*");
                c.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory-Sample");
            });
            services.AddHttpClient("everything", c =>
            {
                c.BaseAddress = new Uri("https://newsapi.org/v2/everything");
                c.DefaultRequestHeaders.Add("Accept", "*/*");
                c.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory-Sample");
            });
            services.AddControllers();
            services.AddAutoMapper(typeof(newsportal.Mapper.Mapper));
            services.AddSwaggerGen(c =>
            {
                // c.OperationFilter<CustomOperationFilter>();
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "NewsPortal Api", Version = "v1" });
                //c.OperationFilter<AuthResponsesOperationFilter>();

                var archivoXmlComentarios = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var rutaApiComentarios = Path.Combine(AppContext.BaseDirectory, archivoXmlComentarios);
                c.IncludeXmlComments(rutaApiComentarios);
            });
            services.AddCors(options =>
            {
                options.AddPolicy("Open",builder => builder.AllowAnyOrigin()
                                                            .AllowAnyMethod()
                                                            .AllowAnyHeader()
                                                            .WithExposedHeaders("X-Pagination"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("Open");
            app.UseStaticFiles();
            app.UseRouting();
            app.UseHttpsRedirection();
            app.UseDeveloperExceptionPage();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => {

                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "NewsPortal");
                    c.RoutePrefix = string.Empty;
                    c.InjectStylesheet("/css/SwaggerDark.css");
                }
                );

            }

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


        }
    }
}
