import React from "react";

function NavBar() {
  return (
    <div className="w3-bar  w3-indigo">
      <a href="/TopN" className="w3-bar-item w3-button  w3-indigo">Top Headlines</a>
      <a href="/Search" className="w3-bar-item w3-button  w3-indigo">Search</a>
    </div>
  );
}

export default NavBar;
