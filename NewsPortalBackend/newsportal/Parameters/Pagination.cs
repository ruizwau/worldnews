﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newsportal.Parameters
{
    public class Pagination
    {
        public int totalPages { get; set; }
        public int pagesize { get; set; }
        public int page { get; set; }
        public int totalResults { get; set; }
        public bool HasPrevious => page > 1;
        public bool HasNext => page < totalPages;
        public Pagination(int count, int pageNumber, int pSize)
        {
            totalResults = count;
            pagesize = pSize;
            page = pageNumber;
            totalPages = (int)Math.Ceiling(count / (double)pSize);
        }
    }
}
