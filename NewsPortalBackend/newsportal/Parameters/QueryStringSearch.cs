﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newsportal.Parameters
{
    public class QueryStringsSearch
    {
        /// <summary>
        /// maxPageSize default value de 50
        /// </summary>
        const int maxPageSize = 100;
        /// <summary>
        /// PageNumber es el númer de página en la que desea posicionarse, por default es 1.
        /// </summary>
        public int Page { get; set; } = 1;

        private int _pageSize = 10;
        /// <summary>
        /// Pagesize es la cantidad de registros por página que por default es de 10 y que admite como máximo 50.
        /// </summary>
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        /// <summary>
        /// SearchTerm
        /// </summary>
        public string keywords { get; set; } = "a";
        /// <summary>
        /// dateFrom por deafult tiene fecha de hoy
        /// </summary>
        public string dateFrom { get; set; } = DateTime.Now.Date.ToString("yyyy-MM-dd");
        /// <summary>
        ///  dateFrom por deafult tiene fecha de hoy
        /// </summary>
        public string dateTo { get; set; } = DateTime.Now.Date.ToString("yyyy-MM-dd");
        /// <summary>
        ///  dateFrom por deafult es español pero puede ser ar, de, en, es, fr, he, it, nl, no, pt, ru, sv, ud, zh
        /// </summary>
        public string language { get; set; } = "es";


    }
}
