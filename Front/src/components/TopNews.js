import React from "react";
import { useContext, useEffect } from "react";
import News from "./News";
import SideBar from './SideBar';
import { GlobalContext } from "../NewsContext/GlobalContext";
import axios from "axios";
function TopN() {

  const [url, seturl] = React.useState(`https://localhost:44379/api/news/top-headlines`);
  const {setData} = useContext(GlobalContext);

  const HandleResponse = (p) => {
    var h = JSON.parse(p.headers['x-pagination']);
    setData(p.data)
  };

  useEffect(() => {
    axios
      .get(url)
      .then((response) => HandleResponse(response))
      .catch((error) => console.log(error));
  }, [url]);
  
  return (
    <div className="news-body">
        <SideBar set={seturl}></SideBar>
        <div className="newscontainer" >
            <News/>
        </div>
    </div>
  );
}

export default TopN;