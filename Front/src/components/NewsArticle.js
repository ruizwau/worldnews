import React from "react";

function NewsArticle({ data }) {
  return (
    <div className="w3-white w3-card" style={{}}>
      <div className="w3-padding">
        <header className="w3-container w3-wide w3-text-indigo">
          <h5>{data.title}</h5>
        </header>
        <div className="">
          <a href={data.url} role="button"><img className="" alt="" src={data.urlToImage} style={{width:"auto",height:"150px"}} /> </a>
 
        </div>
        <div className="w3-container">
          <p>{data.description}</p>
        </div>
        <footer className="w3-container w3-text-grey" style={{display:"inline-flex"}}>
          <h6 className="w3-margin-right">{data.author}</h6>
        </footer>
      </div>
    </div>
  );
}

export default NewsArticle;
