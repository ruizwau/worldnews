﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using newsportal.Models;
using newsportal.Parameters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace newsportal.Controllers
{
    [ApiController]
    [Route("api/news")]
    public class NewsController : Controller
    {

        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public NewsController(IHttpClientFactory httpClientFactory, IMapper mapper,IConfiguration config)
        {
            _httpClientFactory = httpClientFactory;
            _config = config;
            _mapper = mapper;
        }

        /// <summary>
        /// Lista las noticias en base los titulares mas importantes.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Route("~/api/news/top-headlines")]
        [HttpGet]
        public async Task<ActionResult> Get([FromQuery] QueryStrings p)
        {
            string response = "";
            var client = _httpClientFactory.CreateClient(name: "top-headlines");
            var key= _config.GetSection("News:ServiceApiKey").Value;
            try
            {
                response = await client.GetStringAsync($"?country={p.country}&category={p.category}&pageSize={p.PageSize}&page={p.Page}&apiKey={key}");
                var desObject = JsonConvert.DeserializeObject<Response>(response);
                Pagination pg = new Pagination(desObject.totalResults, p.Page, p.PageSize);
                var metada = new
                {
                    pg.totalResults,
                    pg.totalPages,
                    pg.page,
                    pg.pagesize,
                    pg.HasNext,
                    pg.HasPrevious
                };
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metada));
            }
            catch (Exception e)                                                          
            {
                return StatusCode(401,e.Message);
            }

            return Ok(response);
        }
        /// <summary>
        /// Busca una noticia en un rango de de fechas.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Route("~/api/news/search")]
        [HttpGet]
        public async Task<ActionResult> Search([FromQuery] QueryStringsSearch s)
        {
            string response = "";
            var key = _config.GetSection("News:ServiceApiKey").Value;
            if (string.IsNullOrWhiteSpace(s.keywords) || string.IsNullOrWhiteSpace(s.keywords))
            {
                ModelState.AddModelError("", $"Search term cannot be empty");
                return StatusCode(400, ModelState.SelectMany(x => x.Value.Errors.Select(b => b.ErrorMessage)));
            }
            if (!string.IsNullOrWhiteSpace(s.dateFrom) && !string.IsNullOrWhiteSpace(s.dateTo))
            {
                if (!ValidateDate(s.dateFrom) && !ValidateDate(s.dateTo))
                {
                    ModelState.AddModelError("", $"Date Format should be: yyyy-mm-dd");
                    return StatusCode(400, ModelState.SelectMany(x => x.Value.Errors.Select(b => b.ErrorMessage)));
                }
            }
            var client = _httpClientFactory.CreateClient(name: "everything");
            try
            {
                response = await client.GetStringAsync($"?q={s.keywords}&from={s.dateFrom}&to={s.dateTo}&pageSize={s.PageSize}&page={s.Page}&language={s.language}&apiKey={key}");
                var desObject = JsonConvert.DeserializeObject<Response>(response);
                Pagination p = new Pagination(desObject.totalResults, s.Page, s.PageSize);
                var metada = new
                {
                    p.totalResults,
                    p.totalPages,
                    p.page,
                    p.pagesize,
                    p.HasNext,
                    p.HasPrevious
                };
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metada));

            }
            catch (Exception e)
            {
                return StatusCode(401, e.Message);
            }

            return Ok(response);
        }


        private bool ValidateDate(string stringToValidate)
        {
            DateTime dt;
            bool ok = DateTime.TryParseExact(
               stringToValidate,
               "yyyy-MM-dd",
               CultureInfo.InvariantCulture,
               DateTimeStyles.None,
               out dt
            );

            return ok;
        }
    }
}
