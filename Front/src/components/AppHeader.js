import banner from '../images/breaking-news-banner-png-bis.png';
import React, { createContext, useEffect, useState } from "react";
import '../App.css';
function Header() {
    return (
        <><header className="App-header">
            <div className="headerbackground w3-indigo"></div>
        </header><div className="tittle w3-panel  w3-border-indigo">
                <img src={banner} className="App-logo" alt="banner" />
                <h6>
                    Latest News from around the{'  '}
                    <span className="w3-tag w3-indigo w3-spin">world</span>
                </h6>
            </div></>
    );
}

export default Header;
