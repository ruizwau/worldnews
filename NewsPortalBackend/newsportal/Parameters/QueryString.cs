﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newsportal.Parameters
{
    public class QueryStrings
    {
        /// <summary>
        /// maxPageSize default value de 50
        /// </summary>
        const int maxPageSize = 100;
        /// <summary>
        /// Page es el número de página en la que desea posicionarse, por default es 1.
        /// </summary>
        public int Page { get; set; } = 1;

        private int _pageSize = 10;
        /// <summary>
        /// Pagesize es la cantidad de registros por página que por default es de 10 y que admite como máximo 50.
        /// </summary>
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        /// <summary>
        /// País por default es ar pero puede traer las top noticias de otros paises como:ae,ar,at,au,be,bg,br,ca,ch,cn,co,cu,cz,de,eg,fr,gb,gr,hk,hu,id,ie,il,in,it,jp,kr,lt,lv,ma,mx,my,ng,nl,no,nz,ph,pl,pt,ro,rs,
        /// ru,sa,se,sg,si,sk,th,tr,tw,ua,us,ve,za. .
        /// </summary>
        public string country { get; set; } = "ar";
        /// <summary>
        /// Por default la categoría es business pero puede ser otra la categoría como business,entertainment,generalhealth,science,sports and technology.
        /// </summary>
        public string category { get; set; } = "business";

    }
}
