import React, { createContext,useState } from "react";
import moment from 'moment';

export const GlobalContext = createContext();

export const GlobalContextProvider = (props) => {

 
  const [data, setData] = useState();
  const [page, setpage] = useState(1);
  const [pagesize, setpagesize] = useState(10);
  const [totalpages, settotalpages] = useState(0);
  const [hasnext, sethasnext] = useState(false);
  const [hasprev, sethasprev] = useState(false);
  const [total, settotal] = useState(0);
  const[category, setcategory] = useState('business');
  const[country, setcountry] = useState('ar');
  const[SeacrhTerm, setSeacrhTerm] = useState("peso");
  const[language,setlanguage]=useState("es");
  const[dateFrom, setdateFrom] = useState(moment(new Date()).format("YYYY-MM-DD"));
  const[dateTo, setdateTo] = useState(moment(new Date()).format("YYYY-MM-DD"));
  const [url, seturl] = useState(``);
  
  return (
    <GlobalContext.Provider value={{ data,
                                     country,
                                     category,
                                     SeacrhTerm,
                                     language,
                                     dateFrom,
                                     dateTo,
                                     total,
                                     page,
                                     pagesize,
                                     totalpages,
                                     hasnext,
                                     hasprev,
                                     url,
                                     setcountry,
                                     setcategory,
                                     setdateFrom,
                                     setdateTo,
                                     setSeacrhTerm,
                                     setlanguage,                                    
                                     settotal,
                                     setpage,
                                     setpagesize,
                                     settotalpages,
                                     sethasnext,
                                     sethasprev,
                                     setData,
                                     seturl}}>
      {props.children}
    </GlobalContext.Provider>
  );
};
